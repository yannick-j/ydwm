/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = {
    // "DejaVu Sans:pixelsize=16:antialias=true:autohint=true"
    "DejaVuSansM Nerd Font:size=12:antialias=true:autohint=true"
                                      };
static const char dmenufont[]       = "DejaVuSansM Nerd Font:size=12:antialias=true:autohint=true";
static const char col_bg[]          = "#282828";
static const char col_fg[]          = "#ebdbb2";
static const char col_bg2[]         = "#504945";
static const char col_fg2[]         = "#d5c4a1";
static const char col_bg0_h[]       = "#1d2021";
static const char col_darkgray[]    = "#a89984";
static const char col_gray[]        = "#928374";
static const char col_red[]         = "#cc241d";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray,  col_bg0_h, col_bg0_h },
	[SchemeSel]  = { col_fg2,   col_bg2,   col_bg2  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "󰖟", "" };

static const Rule rules[] = {
/* xprop(1):
 *	WM_CLASS(STRING) = instance, class
 *	WM_NAME(STRING) = title
 */
/*  class     instance title         tags mask iscentered isfloating monitor */
    /* { "Gimp", NULL,    NULL,         0,        0,         1,         -1 }, */
    { "Gcr-prompter", NULL, NULL,    0,        1,         1,         -1 },
    { NULL,   NULL,    "mocp",       1 << 8,   0,         0,         -1 },
    { NULL,   NULL,    "pulsemixer", 1 << 8,   0,         0,         -1 },
    { NULL,   NULL,    "cava",       1 << 8,   0,         0,         -1 },
    { "qutebrowser", NULL, NULL,     1 << 7,   0,         0,         -1 },
    { "Firefox-esr", NULL, NULL,     1 << 6,   0,         0,         -1 },
    { "Chromium", NULL, NULL,     1 << 7,   0,         0,         -1 },
    { "Brave-browser", NULL, NULL,     1 << 7,   0,         0,         -1 },
    { "LibreWolf", NULL, NULL,     1 << 7,   0,         0,         -1 },
};

/* layout(s) */
static const float mfact = 0.58; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ NULL,       NULL },
};

static const Taglayout taglayouts[] = {
	/* tag		layout */
	{ 9,		{.v = &layouts[3]} },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY, view,       {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY, toggleview, {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY, tag,        {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY, toggletag,  {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "rofi", "-modi", "run,drun,window,emoji:rofimoji,nerd-fonts:rofimoji -f nerd_fonts", "-show", "drun", "-show-icons", "-sidebar-mode", NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *filecmd[]  = { "alacritty", "-e", "ranger", NULL };
// static const char *filecmd[]  = { "lxterminal", "-e", "lf", NULL };
static const char *editorcmd[]  = { "alacritty", "-e", "nvim", NULL };
static const char *webcmd[]  = { "librewolf", NULL };
static const char *mailcmd[]  = { "alacritty", "-e", "neomutt", NULL };
static const char *quitcmd[] = { "dmenu-power-menu", NULL };
static const char *mountcmd[]  = { "dmenu-mount", NULL };
static const char *unmountcmd[]  = { "dmenu-unmount", NULL };
static const char *synccmd[]  = { "alacritty", "-e", "dmenu-rsync", NULL };
static const char *audiocmd[]  = { "my_audio.sh", NULL };
static const char *audioplaycmd[]  = { "mocp", "--toggle-pause", NULL };
static const char *audionextcmd[]  = { "mocp", "--next", NULL };
static const char *audioprevcmd[]  = { "mocp", "--previous", NULL };
static const char *audioupcmd[]  = { "amixer", "sset", "Master", "5%+", NULL };
static const char *audiodowncmd[]  = { "amixer", "sset", "Master", "5%-", NULL };
static const char *audiomutecmd[]  = { "amixer", "sset", "Master", "toggle", NULL };
static const char *mixercmd[]  = { "alacritty", "-e", "pulsemixer", NULL };
static const char *passcmd[]  = { "passmenu", NULL };
// static const char *helpcmd[]  = { "dwm-cheatsheet.sh", NULL };

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,           XK_t,            spawn,          {.v = editorcmd } },
	{ MODKEY,           XK_w,            spawn,          {.v = webcmd } },
	{ MODKEY,           XK_d,            spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask, XK_q,            spawn,          {.v = quitcmd } },
	{ MODKEY,           XK_m,            spawn,          {.v = mountcmd } },
	{ MODKEY,           XK_u,            spawn,          {.v = unmountcmd } },
	{ MODKEY,           XK_f,            spawn,          {.v = filecmd } },
	{ MODKEY,           XK_e,            spawn,          {.v = mailcmd } },
	{ MODKEY,           XK_s,            spawn,          {.v = synccmd } },
	{ MODKEY,           XK_a,            spawn,          {.v = audiocmd } },
	{ MODKEY|ShiftMask, XK_a,            spawn,          {.v = mixercmd } },
	{ MODKEY|ShiftMask, XK_p,            spawn,          {.v = passcmd } },
	{ MODKEY,           XK_Return,       spawn,          {.v = termcmd } },
	// { MODKEY,           XK_c,            spawn,          {.v = helpcmd } },
	{ MODKEY,           XK_b,            spawn,          SHCMD("dmenu-bluetooth") },
	{ MODKEY|ShiftMask, XK_b,            togglebar,      {0} },
	{ MODKEY,           XK_j,            focusstack,     {.i = +1 } },
	{ MODKEY,           XK_k,            focusstack,     {.i = -1 } },
	{ MODKEY,           XK_bracketright, incnmaster,     {.i = +1 } },
	{ MODKEY,           XK_bracketleft,  incnmaster,     {.i = -1 } },
	{ MODKEY,           XK_h,            setmfact,       {.f = -0.01} },
	{ MODKEY,           XK_l,            setmfact,       {.f = +0.01} },
	{ MODKEY,           XK_c,            zoom,           {0} },
	{ MODKEY,           XK_Tab,          view,           {0} },
	{ MODKEY,           XK_q,            killclient,     {0} },
	{ MODKEY|ShiftMask, XK_t,            setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask, XK_f,            setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask, XK_m,            setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ShiftMask, XK_u,            setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask, XK_o,            setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ShiftMask, XK_c,        cyclelayout,    {.i = +1 } },
	{ MODKEY|ShiftMask, XK_Return,       togglefloating, {0} },
	{ MODKEY,           XK_0,            view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask, XK_0,            tag,            {.ui = ~0 } },
	{ MODKEY,           XK_comma,        focusmon,       {.i = -1 } },
	{ MODKEY,           XK_period,       focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask, XK_comma,        tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask, XK_period,       tagmon,         {.i = +1 } },
	TAGKEYS(            XK_1,                            0)
	TAGKEYS(            XK_2,                            1)
	TAGKEYS(            XK_3,                            2)
	TAGKEYS(            XK_4,                            3)
	TAGKEYS(            XK_5,                            4)
	TAGKEYS(            XK_6,                            5)
	TAGKEYS(            XK_7,                            6)
	TAGKEYS(            XK_8,                            7)
	TAGKEYS(            XK_9,                            8)
	{ MODKEY|ControlMask|ShiftMask, XK_q, quit,          {0} },
    /* X86AudioPlay */
	{ 0,                0x1008ff14,      spawn,          {.v = audioplaycmd} },
	{ 0,                0x1008ff13,      spawn,          {.v = audioupcmd} },
	{ 0,                0x1008ff11,      spawn,          {.v = audiodowncmd} },
	{ 0,                0x1008ff12,      spawn,          {.v = audiomutecmd} },
	{ 0,                0x1008ff17,      spawn,          {.v = audionextcmd} },
	{ 0,                0x1008ff16,      spawn,          {.v = audioprevcmd} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

