Raccourcis clavier de DWM
=========================
Commandes de base
-----------------
󰘳 󰌑     Ouvrir un terminal
󰘳 p     Menu
󰘳 q     Fermer une fenêtre
󰘳 j/k   Focus sur la fenêtre suivante/précédente
󰘳 󱁐     Zoom
Navigation
----------
󰘳 󰘶 b   Montrer/Cacher la barre
󰘳 h/l   Redimensionner la fenêtre
󰘳 1-9   Aller au bureau 1-9
󰘳 󰌒     Aller au bureau précédent
󰘳 󰘶 1-9 Déplacer la fenêtre vers le bureau 1-9
󰘳 [ / ] Augmenter/Diminuer le nombre de fenêtres maîtresses
Lanceurs
--------
󰘳 t     editorcmd
󰘳 w     webcmd
󰘳 󰘶 q   quitcmd
󰘳 m     mountcmd
󰘳 u     unmountcmd
󰘳 r     filecmd
󰘳 e     mailcmd
󰘳 s     synccmd
󰘳 a     audiocmd
󰘳 󰘶 a   mixercmd
󰘳 󰘶 p   passcmd
Layouts
-------
󰘳 󰘶 t   setlayout 0
󰘳 󰘶 f   setlayout 1
󰘳 󰘶 m   setlayout 2
󰘳 󰘶 u   setlayout 3
󰘳 󰘶 󰌑   cyclelayout
󰘳 󰘶 󱁐   togglefloating
󰘳 0     view 0
󰘳 󰘶 0   tag 0
󰘳 ,     focusmon -1
󰘳 .     focusmon +1
󰘳 󰘶 ,   tagmon{.i = -1 }
󰘳 󰘶 .   tagmon{.i = +1 }
󰘳 󰘴 󰘶 q quit
