# ydwm

My build of [dwm](https://dwm.suckless.org).

# Installation initiale

```sh
sudo apt install build-essential libx11-dev libxft-dev libxinerama-dev
git clone https://git.suckless.org/dwm
mv dwm ydwm
cd ydwm
rm -rf .git
git init
git remote add origin git@gitlab.com:yannick-j/ydwm.git
git add .
git commit -m "initial"
git push --set-upstream origin main
```

# Patches

```sh
mkdir patches
```

- [attachaside 6.3](https://dwm.suckless.org/patches/attachaside/)
- [bottomstack 6.1](https://dwm.suckless.org/patches/bottomstack/)
- [cyclelayouts 20180524-6.2](https://dwm.suckless.org/patches/cyclelayouts/)
- [forcelayout 6.1](https://github.com/snhilde/dwm-forcelayout)
- [pertag 20200914-61bb8b2](https://dwm.suckless.org/patches/pertag/)
- [status2d 6.3](https://dwm.suckless.org/patches/status2d/)

Copier les fichiers téléchargés dans `patches/`

```sh
git branch yannick
git checkout yannick
mv README README.md
git add .
git commit -m "ajout patches/ et README.md"
```

Appliquer les patches dans l'ordre en corrigant les erreurs éventuelles.

```sh
patch -p1 < patches/dwm-attachaside-6.3.diff
patch -p1 < patches/dwm-bottomstack-6.1.diff
patch -p1 < patches/dwm-cyclelayouts-20180524-6.2.diff
patch -p1 < patches/dwm-forcelayout-6.1.diff
patch -p1 < patches/dwm-pertag-20200914-61bb8b2.diff 
patch -p1 < patches/dwm-status2d-6.3.diff
```

Faire le ménage et modifier les fichiers `config.h` et `config.def.h`

```sh
rm config.def.h.orig config.def.h.rej dwm.c.orig dwm.c.rej
cp ../dwm_old/config.h ./
cp config.h config.def.h
sudo make clean install
```

```sh
git add .
git commit -m "patch -p1 attachaside bottomstack cyclelayout forcelayout pertag status2d et maj README.md"
git push
```

# Dépendances

## st

```sh
git clone https://gitlab.com/yannick-j/yst.git
```

## paquets debian

```sh
sudo apt install rofi qutebrowser moc cava pulsemixer
```

dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
